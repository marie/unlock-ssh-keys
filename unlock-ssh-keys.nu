#!/usr/bin/env nu

def getBitwardenSession [] {
    if ('BW_SESSION' in $env) {
        return $env.BW_SESSION
    }

    do {
        bw login --check --quiet
    }

    let operation = if ($env.LAST_EXIT_CODE == 1) {
        "login"
    } else {
        "unlock"
    }

    let session = (bw --raw $operation)

    if ('PRINT_BW_SESSION' in $env) {
        print "Successfully logged into bitwarden. To reuse this session run:"
        print $"$env.BW_SESSION=\"($session)\""
    } else {
        print "Successfully logged into bitwarden."
    }
    return $session
}

def getFolderId [
    folderName: string
] {
    let folders = (bw list folders --search $"($folderName)") | from json
    if ($folders | is-empty) {
        print "No such folder"
        exit 1
    }

    if (($folders | length) > 1) {
        print "More than one folder has been found."
        exit 1
    }

    let folder = $folders | first

    print $"Using folder ($folder.name)"

    let items = (bw list items --folderid $"($folder.id)") | from json

    for $item in $items {
        let path = $"~/.ssh/($item.name)" | path expand
        if ($path | path exists) {
            print $"Loading ($item.name)"
            $env.SSH_KEY_PASSPHRASE = $item.login.password
            $env.SSH_ASKPASS = $"($env.FILE_PWD)/print-password.nu"
            $env.SSH_ASKPASS_REQUIRE = "force"
            $env.SSH_ASKPASSWORD = ''
            ssh-add $'($path)'
        } else {
            print $"Couldn't find ($path)"
        }
    }
}

def main [
    --sshDir: string = "~/.ssh",
    --folderName: string,
] {
    mut folderName = $folderName
    if ($folderName | is-empty) {
        if ('BW_FOLDER_NAME' in $env) {
            $folderName = $env.BW_FOLDER_NAME
        } else {
            print "--folderName or the environment variable BW_FOLDER_NAME is required."
            exit 1
        }
    }

    ls $sshDir | get name

    let session = getBitwardenSession
    $env.BW_SESSION = $session
    getFolderId $folderName
}
