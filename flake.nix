{
  description = "SSH agent unlocking using your Bitwarden Vault";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } ({ withSystem, ... }: {
      imports = [ ];
      systems = [ "x86_64-linux" "aarch64-linux" "aarch64-darwin" "x86_64-darwin" ];
      perSystem = { config, self', inputs', pkgs, system, ... }: {
        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [ nushell bitwarden-cli ];
        };
        packages.default = pkgs.stdenvNoCC.mkDerivation {
          name = "unlock-ssh-keys";
          version = "0.1.0";

          src = pkgs.lib.sourceFilesBySuffices ./. [".nu"];

          dontConfigure = true;
          dontBuild = true;

          installPhase = ''
            runHook preInstall
            mkdir -p $out/share/unlock-ssh-keys $out/bin
            cp -r $src/* $out/share/unlock-ssh-keys
            makeWrapper $out/share/unlock-ssh-keys/unlock-ssh-keys.nu $out/bin/unlock-ssh-keys \
              --prefix PATH : ${pkgs.lib.makeBinPath (with pkgs; [ bitwarden-cli ]) }
            runHook postInstall
          '';

          buildInputs = with pkgs; [ nushell ];
          nativeBuildInputs = with pkgs; [ makeWrapper ];

          meta = {
            description = "SSH Agent unlock script using bitwarden written in nu";
            homepage = "https://codeberg.org/marie/unlock-ssh-keys";
            license = pkgs.lib.licenses.gpl3;
            maintainers = with pkgs.lib.maintainers; [ marie ];
            platforms = pkgs.nushell.meta.platforms;
          };
        };
      };
      flake = {
        homeManagerModules.default = { pkgs, ... }: {
          imports = [ ./home-manager.nix ];
          programs.unlock-ssh-keys.package = withSystem pkgs.stdenv.hostPlatform.system ({ config, ... }:
            config.packages.default
          );
        };
      };
    });
}
