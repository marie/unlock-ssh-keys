# unlock-ssh-keys

[![Written in Nushell](https://img.shields.io/badge/Nushell-3aa675?style=flat-square&label=Written%20in&color=%233aa675)](https://nushell.sh)
[![Bitwarden](https://img.shields.io/badge/Bitwarden-white?style=flat-square&logo=bitwarden&logoColor=white&labelColor=%23175DDC&color=%23175DDC)](https://bitwarden.com)


Simple [Nushell](https://nushell.sh) script to automatically load your ssh keys into your ssh-agent using passwords stored in your [Bitwarden](https://bitwarden.com) vault.

## Installation

### Nix

Install unlock-ssh-keys using the [Nix](https://nixos.org/nix) package manager.
<details>
<summary>Temporary Shell using `nix shell`</summary>

```shell
nix shell git+https://codeberg.org/marie/unlock-ssh-keys
```
</details>

<details>
<summary>Install using `nix profile`</summary>

```shell
nix profile install git+https://codeberg.org/marie/unlock-ssh-keys
```
</details>

<details>
<summary>Install using <a href="https://github.com/nix-community/home-manager" rel="noreferrer noopener" target="_blank">home-manager</a></summary>

Add the flake as an input to your flake:

```nix
{
  # ...
  inputs.unlock-ssh-keys = {
    url = "git+https://codeberg.org/marie/unlock-ssh-keys";
    # the following 2 lines are optional, but they reduce dependencies
    inputs.nixpkgs.follows = "nixpkgs";
    # If you don't use flake-parts, don't include this line
    inputs.flake-parts.follows = "flake-parts";
  };
  outputs = inputs@{ nixpkgs, home-manager, ... }: {
    homeConfigurations.example = home-manager.lib.homeManagerConfiguration {
      modules = [
        inputs.unlock-ssh-keys.homeManagerModules.default
        {
          programs.unlock-ssh-keys = {
            enable = true;
            settings = {
              # The default bitwarden folder to use
              folder = "SSH Keys";
              # Print the bitwarden session key for reuse
              printSession = true;
            };
          };
        }
      ];
    };
  };
}
```

</details>

## Usage

> Note:
> If you have an existing bitwarden-cli session, make sure `BW_SESSION` is set correctly.

Run `unlock-ssh-keys --folderName "<Name of the bitwarden folder with your SSH Key passwords>"`

## Configuration

| Environment variable | Description                                      | Default Value |
|----------------------|--------------------------------------------------|---------------|
| `PRINT_BW_SESSION`   | Prints the bitwarden session key after login     | `false`       |
| `BW_SESSION`         | Use an existing bitwarden session                | None          |
| `BW_FOLDER_NAME`     | Provide a default value for the bitwarden folder | None          |

## Security

If you find a security vulnerability, please DO NOT open an issue. Instead contact me via [matrix](https://matrix.to/#/@marie:marie.cologne).

## License
This repository is licensed under the [GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0) or later (GPL-3.0-or-later). By contributing to this project, you agree that your contributions will also be subject to this license.
