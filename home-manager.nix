{ config, lib, pkgs, ... }:
let
  cfg = config.programs.unlock-ssh-keys;
  wrapper = pkgs.stdenvNoCC.mkDerivation {
    name = "unlock-ssh-keys-wrapped";

    dontUnpack = true;
    dontConfigure = true;
    dontBuild = true;
    dontFixup = true;

    nativeBuildInputs = with pkgs; [ makeWrapper ];

    installPhase = ''
      runHook preInstall
      mkdir -p $out/bin
      makeWrapper ${cfg.package}/bin/unlock-ssh-keys $out/bin/unlock-ssh-keys \
        ${lib.optionalString (cfg.settings.folder != null) "--set BW_FOLDER_NAME ${lib.escapeShellArg cfg.settings.folder}"} \
        ${lib.optionalString (cfg.settings.printSession) "--set PRINT_BW_SESSION true"}

      runHook postInstall
    '';
  };
in
{
  options.programs.unlock-ssh-keys = {
    enable = lib.mkEnableOption "unlock-ssh-keys";
    package = lib.mkOption {
      type = lib.types.package;
      description = "The unlock-ssh-keys package to use";
    };
    settings = {
      folder = lib.mkOption {
        type = with lib.types; nullOr str;
        default = null;
        description = "The default bitwarden folder to use";
      };
      printSession = lib.mkOption {
        type = lib.types.bool;
        default = false;
        description = "Weither it should print the bitwarden session.";
      };
    };
  };
  config.home.packages = lib.mkIf cfg.enable [ wrapper ];
}
